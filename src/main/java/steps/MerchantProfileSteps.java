package steps;

import com.codeborne.selenide.Condition;
import page.MerchantProfilePage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;
import static constants.Constants.*;

public class MerchantProfileSteps {

    public static final MerchantProfilePage mercnahtProfilePage = new MerchantProfilePage();

    public MerchantProfileSteps openPage(String url) {
        return open(url, MerchantProfileSteps.class);
    }

    public void openProfile() {
        openPage(API_URL + MERCHANT_PROFILE_PAGE + MERCHANT_ID);
        mercnahtProfilePage.merchantIdTitle.shouldHave(Condition.text(MERCHANT_ID));
    }

    public void createBonusTransaction() {
        mercnahtProfilePage.walletTransactionTab.click();
        mercnahtProfilePage.tabTitle.shouldHave(Condition.text(CARD_WALLET_TITLE));
        mercnahtProfilePage.addTransaction.click();
        mercnahtProfilePage.newTransactionTitle.shouldHave(Condition.text(NEW_TRANSACTION_TITLE));
        mercnahtProfilePage.amount.setValue(NEW_PRODUCT_PRICE);
        mercnahtProfilePage.operation.click();
        mercnahtProfilePage.description.setValue(REASON_TRANSACTION);
        mercnahtProfilePage.addBtn.click();
    }

    public String createNewProduct() {
        mercnahtProfilePage.productTab.click();
        mercnahtProfilePage.productTabTitle.shouldHave(Condition.text(PRODUCT_TAB_TITLE));
        mercnahtProfilePage.newProduct.click();
        mercnahtProfilePage.nameProductEn.setValue(PRODUCT_NAME_EN);
        mercnahtProfilePage.nameProductAr.setValue(PRODUCT_NAME_AR);
        mercnahtProfilePage.price.setValue(PRODUCT_PRICE);
        mercnahtProfilePage.tags.setValue(PRODUCT_TAG);
        mercnahtProfilePage.tagChoose.click();
        mercnahtProfilePage.category.setValue(PRODUCT_CATEGORY);
        mercnahtProfilePage.categoryChoose.click();
        mercnahtProfilePage.saveProductBtn.click();
        mercnahtProfilePage.succesfulNotification.shouldHave(Condition.text(SUCCEDDFULL_NOTIFICATION));
        String productId = mercnahtProfilePage.productId.getAttribute(TITLE);
        return productId;
    }

    public void cancelOrder(String orderId) {
        openPage(API_URL + ORDER_URL + orderId);
        mercnahtProfilePage.orderPageTitle.shouldHave(text(ORDER_TITLE));
        mercnahtProfilePage.cancelOrderBtn.click();
        mercnahtProfilePage.cancelReasonTab.shouldBe(Condition.visible);
        mercnahtProfilePage.reason.setValue(CANCEL_REASON);
        mercnahtProfilePage.saveCancelReasonBtn.click();
        mercnahtProfilePage.orderCanceledNotification.shouldBe(Condition.visible).shouldHave(Condition.text(ORDER_SUCCESSFULL_UPDATE));
    }

}
