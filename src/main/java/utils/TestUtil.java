package utils;

import domain.*;

import java.util.List;

import static constants.Constants.*;

public class TestUtil {
    public Credential buildCredential(String email, String password) {
        return Credential.builder()
                .email(email)
                .password(password)
                .build();
    }

    public Order buildCustomerOrder(String productId) {
        return Order.builder()
                .catalogOrderItems(List.of(new CatalogOrder(productId,1)))
                .comment(COMMENT)
                .deliveryTime(DELIVERY_TIME)
                .dropOff(buildDropOff())
                .orderKind(ORDER_KIND)
                .payment(buildPayment())
                .pickUp(buildPickUp())
                .operationArea(OPERATION_AREA)
                .build();
    }


    public Payment buildPayment() {
        return Payment.builder()
                .kind(PAYMENT_KIND)
                .useClientBalance(true)
                .build();
    }

    public Place buildDropOff() {
        return Place.builder()
                .address(buildDropOffAddress())
                .kind(PLACE_KIND)
                .build();
    }

    public DropOff buildDropOffAddress() {
        return DropOff.builder()
                .formattedAddress(DROPOFF_ADDRESS)
                .geoPoint(buildGeoPoint(DROP_OFF_LAT,DROP_OFF_LON))
                .build();
    }


    private PickUp buildPickUp() {
        return PickUp.builder()
                .address(buildPickUpAddress())
                .kind(PICKUP_KIND)
                .merchantPos(merchantPosId())
                .build();
    }

    public Address buildPickUpAddress() {
        return Address.builder()
                .formattedAddress(PICKUP_ADDRESS)
                .area(AREA)
                .geoPoint(buildGeoPoint(PICK_UP_LAT,PICK_UP_LON))
                .placeName(NAME)
                .build();
    }

    public MerchantPos merchantPosId(){
        return MerchantPos.builder()
                .posId(POS_ID)
                .build();

    }
    public GeoPoint buildGeoPoint(float lat, float lon) {
        return GeoPoint.builder()
                .lat(lat)
                .lon(lon)
                .build();
    }
}
