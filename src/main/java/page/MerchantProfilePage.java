package page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class MerchantProfilePage {


    public MerchantProfilePage() {
        super();
    }

    public SelenideElement merchantIdTitle = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/span");
    public SelenideElement walletTransactionTab = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/nav/ul/li[5]/a");
    public SelenideElement tabTitle = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/ng-component/div/div[1]/h2");
    public SelenideElement addTransaction = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/ng-component/div/div[1]/button");
    public SelenideElement newTransactionTitle = $x("/html/body/ngb-modal-window/div/div/form/div[1]/h4");
    public SelenideElement amount = $x("/html/body/ngb-modal-window/div/div/form/div[2]/div[1]/div[1]/input");
    public SelenideElement operation = $x("/html/body/ngb-modal-window/div/div/form/div[2]/div[1]/div[3]/select/option[1]");
    public SelenideElement description = $x("/html/body/ngb-modal-window/div/div/form/div[2]/div[2]/textarea");
    public SelenideElement addBtn = $x("/html/body/ngb-modal-window/div/div/form/div[3]/button[2]");

    public SelenideElement productTab = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/nav/ul/li[3]/a");
    public SelenideElement productTabTitle = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-list-page/div/div[1]/h2");
    public SelenideElement newProduct = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-list-page/div/div[1]/a");
    public SelenideElement nameProductEn = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/form/div/div[1]/div[2]/div[1]/div[1]/input");
    public SelenideElement nameProductAr = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/form/div/div[1]/div[2]/div[1]/div[2]/input");
    public SelenideElement price = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/form/div/div[1]/div[3]/div/div[2]/div/input");
    public SelenideElement tags = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/form/div/div[2]/div[2]/div/am-tag-multi-select/kendo-multiselect/div/kendo-searchbar/input");
    public SelenideElement tagChoose = $x("/html/body/am-app/kendo-popup/div/kendo-list/div/ul/li/table/tbody/tr/td[1]");
    public SelenideElement category = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/form/div/div[3]/div[2]/div/div[1]/am-category-combobox/am-combobox/kendo-combobox/span/kendo-searchbar/input");
    public SelenideElement categoryChoose = $x("/html/body/am-app/kendo-popup/div/kendo-list/div/ul/li");
    public SelenideElement orderPageTitle = $x("//am-order-sections/div[1]/div[2]/h1/span");
    public SelenideElement saveProductBtn = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-merchant-sections/div/am-product-details/div/div/am-save-cancel/div/button[2]");
    public SelenideElement succesfulNotification = $x("/html/body/am-app/div/simple-notifications/div/simple-notification[1]/div/div[1]/div[2]");
    public SelenideElement cancelOrderBtn = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-order-sections/div[2]/ng-component/div/form/div/div/div[2]/div[2]/div[2]/section[2]/div/div/button[2]");
    public SelenideElement cancelReasonTab = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-order-sections/div[2]/ng-component/div/form/div/div/div[2]/div[2]/div[2]/section[2]/am-cancel-order-dialog/form/div/div/div");
    public SelenideElement reason = $x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-order-sections/div[2]/ng-component/div/form/div/div/div[2]/div[2]/div[2]/section[2]/am-cancel-order-dialog/form/div/div/div/div[2]/div/div/div/textarea");
    public SelenideElement saveCancelReasonBtn=$x("/html/body/am-app/div/div/main/am-pages/div/article/div/am-order-sections/div[2]/ng-component/div/form/div/div/div[2]/div[2]/div[2]/section[2]/am-cancel-order-dialog/form/div/div/div/div[3]/button[1]");
    public SelenideElement orderCanceledNotification  = $x("/html/body/am-app/div/simple-notifications/div/simple-notification/div");
    public SelenideElement productId = $x("/html/body/am-app/div/div/main/am-pages/div/header/div[2]/am-breadcrumb/div/span[5]/strong");



}
