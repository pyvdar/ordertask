package domain;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor

public class GeoPoint {
    private float lat;
    private float lon;
}
