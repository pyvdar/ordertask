package page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class AdminPage {
    public AdminPage() {
        super();
    }
    public SelenideElement adminLogin = $x("//*[@id='email']");
    public SelenideElement adminPassword = $x("//*[@id='password']");
    public SelenideElement submitBtn = $x("//*[@id='submit']");
    public SelenideElement dashboardTitle = $("div.card-header");

    public SelenideElement orderIdInput = $x("//am-dashboard/div/div[2]/div/div/div[1]/div/input");
//    public SelenideElement orderPageTitle = $x("//am-order-sections/div[1]/div[2]/h1/span");

}
