import org.junit.Test;
import steps.ApiService;
import steps.LoginPage;
import steps.MerchantProfileSteps;

import java.util.HashMap;

import static constants.Constants.*;

public class MainTest {
    private final LoginPage loginPage = new LoginPage();
    private final MerchantProfileSteps merchantProfileSteps = new MerchantProfileSteps();
    private final ApiService apiService = new ApiService();


    @Test
    public void createTask(){
        System.out.println(" Login as admin");
        loginPage.loginAsAdmin();

        System.out.println("Open merchants profile-card");
        merchantProfileSteps.openProfile();

        System.out.println("Add bonus transaction for merchant");
        merchantProfileSteps.createBonusTransaction();
        System.out.println("Create new product");
        String productId = merchantProfileSteps.createNewProduct();

        System.out.println("Login as customer");
        HashMap<String, Object> customerHeader = apiService.headers(apiService.login(CUSTOMER_EMAIL, CUSTOMER_PASSWORD, USER_AUTHTOKEN));

        System.out.println("Find created product");
        apiService.findProduct(productId);

        System.out.println("Create order");
        String createdOrderId = apiService.createOrderAsCustomer(customerHeader, productId);

        System.out.println("Change pickup-dropoff location");
        HashMap<String, Object> adminHeader = apiService.headers(apiService.login(ADMIN_EMAIL, ADMIN_PASSWORD, "/user/v1/systemuser/authtoken"));
        apiService.changeDropoffLocation(adminHeader,createdOrderId);

        System.out.println("Cancel order by admin");
        merchantProfileSteps.cancelOrder(createdOrderId);
    }
}
