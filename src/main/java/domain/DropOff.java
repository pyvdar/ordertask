package domain;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class DropOff {

    private String formattedAddress;

    private GeoPoint geoPoint;

}
