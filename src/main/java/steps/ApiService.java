package steps;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Credential;
import domain.DropOff;
import domain.GeoPoint;
import domain.Order;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.SneakyThrows;
import utils.TestUtil;

import java.util.HashMap;
import java.util.Map;

import static constants.Constants.*;

public class ApiService {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final TestUtil testUtil = new TestUtil();


    public Response postRequest(String url, Map<String, Object> headers, Object body) {
        RequestSpecification requestSpecification = RestAssured.given();

        for (Map.Entry<String, Object> header : headers.entrySet()) {
            requestSpecification.header(header.getKey(), header.getValue());
        }

        requestSpecification.contentType(CONTENT_TYPE);

        if (body != null) {
            requestSpecification.body(body);
        }

        return requestSpecification.post(url);
    }

    public Response getRequest(String url, Map<String, Object> headers) {
        RequestSpecification requestSpecification = RestAssured.given();
        headers.put(ACCEPT_LANG, EN);
        for (Map.Entry<String, Object> header : headers.entrySet()) {
            requestSpecification.header(header.getKey(), header.getValue());
        }


        return requestSpecification.get(url);
    }
    public Response putRequest(String url, Map<String, Object> headers, String body) {
        RequestSpecification requestSpecification = RestAssured.given();

        for (Map.Entry<String, Object> header : headers.entrySet()) {
            requestSpecification.header(header.getKey(), header.getValue());
        }
        requestSpecification.contentType(CONTENT_TYPE);

        if (body != null) {
            requestSpecification.body(body);
        }
        return requestSpecification.when().put(url);
    }

    @SneakyThrows
    public void changeDropoffLocation(HashMap<String, Object> headers, String orderId) {
        GeoPoint changeDropOffLocation = testUtil.buildGeoPoint(NEW_DROPOFF_LAT, NEW_DROPOFF_LON);
        String body = objectMapper.writeValueAsString(changeDropOffLocation);
        Response response =
                putRequest(API_URL + CHANGE_DROPOFF_LOCATION+orderId+DROPOFF_URL_LAST, headers, body);
        response.then().statusCode(200);

    }

    @SneakyThrows
    public void findProduct(String productId) {
                getRequest(API_URL + FIND_PRODUCT+ productId + CURRENCY_ENDPOINT,new HashMap<>());

    }

    public HashMap<String, Object> headers(String token) {
        HashMap<String, Object> header = new HashMap<>();
        header.put(AUTHORIZATION, token);
        return header;
    }

    @SneakyThrows
    public String login(String email, String password, String auth_endpoint) {
        Credential credential = testUtil.buildCredential(email, password);
        String body = objectMapper.writeValueAsString(credential);
        Response response = postRequest(API_URL + auth_endpoint, new HashMap<>(), body);
        JsonNode token = objectMapper.readTree(response.getBody().asString()).get(AUTH_TOKEN_KEY);
        return token.asText();
    }

    @SneakyThrows
    public String createOrderAsCustomer(HashMap<String, Object> headers, String productId) {
        Order order = testUtil.buildCustomerOrder(productId);
        String body = objectMapper.writeValueAsString(order);
        Response response = postRequest(API_URL + ORDER_ENDPOINT, headers, body);
        JsonNode orderId = objectMapper.readTree(response.getBody().asString()).get(ID_KEY);
        return orderId.asText();
    }
}
