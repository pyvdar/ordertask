package steps;

import page.AdminPage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static constants.Constants.*;

public class LoginPage {
    public static final AdminPage adminPage = new AdminPage();

    public LoginPage openPage(String url) {
        return open(url, LoginPage.class);
    }

    public void loginAsAdmin() {
        openPage(API_URL);
        setAdminLoginCredentials(ADMIN_EMAIL, ADMIN_PASSWORD);
        dashboardTitleShouldBeVisible();
    }

    public void setAdminLoginCredentials(String email, String pass) {
        adminPage.adminLogin.setValue(email);
        adminPage.adminPassword.setValue(pass);
        adminPage.submitBtn.click();
    }

    public void dashboardTitleShouldBeVisible() {
        adminPage.dashboardTitle.shouldBe(visible);
    }

}
