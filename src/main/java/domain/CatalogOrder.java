package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Data
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CatalogOrder {

    private List<String> optionValueIds = new ArrayList<>();

    private String productId;

    private int quantity;

    public CatalogOrder(String productId, int quantity) {
        this.optionValueIds = new ArrayList<>();
        this.productId = productId;
        this.quantity = quantity;
    }
}
