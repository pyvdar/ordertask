package domain;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class PickUp {

    public Address address;

    private String kind;

    private MerchantPos merchantPos;
}
