package domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class Order {

    @Builder.Default
    private List<CatalogOrder> catalogOrderItems = new ArrayList<>();

    private String comment;

    private String deliveryTime;

    private Place dropOff;

    private String orderKind;

    private Payment payment;

    private PickUp pickUp;

    private String operationArea;
}