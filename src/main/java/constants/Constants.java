package constants;

public class Constants {
    public static final String API_URL = "https://sand.toyou.delivery";
    public static final String ADMIN_EMAIL = "admin@arammeem.com";
    public static final String ADMIN_PASSWORD = "0000";

    public static final String CUSTOMER_EMAIL = "dbiskota@gmail.com";
    public static final String CUSTOMER_PASSWORD = "0000";

    public static final String MERCHANT_ID = "03d131b3-a261-4706-becf-aab98a716892";
    public static final String MERCHANT_PROFILE_PAGE = "/dashboard/#/pages/merchants/";
    public static final String CARD_WALLET_TITLE = "Transaction list";
    public static final String NEW_TRANSACTION_TITLE = "New Transaction";
    public static final String PRODUCT_TAB_TITLE = "Product list";

    public static final String ORDER_ENDPOINT = "/delivery/v15/orders";
    public static final String ID_KEY = "id";
    public static final String USER_AUTHTOKEN = "/user/v1/user/authtoken";
    public static final String AUTH_TOKEN_KEY = "authToken";
    public static final String OPERATION_AREA = "Kha_test";
    public static final String ORDER_KIND = "M2ME_REGULAR";

    public static final float DROP_OFF_LAT = 50.20818421328751f;
    public static final float DROP_OFF_LON = 36.42154689878225f;

    public static final float PICK_UP_LAT = 50.182954159398676f;
    public static final float PICK_UP_LON = 36.43366765370683f;

    public static final float NEW_DROPOFF_LAT = 50.213486f;
    public static final float NEW_DROPOFF_LON = 36.414333f;
    public static final String ORDER_TITLE = "Order";

    public static final String COMMENT = " ";
    public static final String DELIVERY_TIME = "1611822457";

    public static final String PAYMENT_KIND = "CASH";
    public static final String PLACE_KIND = "ME";

    public static final String DROPOFF_ADDRESS = "Vul. Pushkinsʹka, 109, Lyptsi, Kharkivs'ka oblast, Ukraine, 62414";
public static final String PICKUP_KIND = "POS_ID";
    public static final String PICKUP_ADDRESS ="вул. Миру, Слобожанське, Харківська область, Украина, 62415";
public static final String AREA = "";
    public static final String NAME = "Name";
    public static final String POS_ID = "95b729c5-b649-4a65-be99-b099bb565b04";
    public static final String NEW_PRODUCT_PRICE = "1000";
    public static final String REASON_TRANSACTION = "prost";
    public static final String PRODUCT_NAME_EN = "kotiky_last";
    public static final String PRODUCT_NAME_AR = "kotiky_last";
    public static final String PRODUCT_PRICE = "1000";
    public static final String PRODUCT_TAG = "tapki_tag";
    public static final String PRODUCT_CATEGORY = "kotyatki_category";
    public static final String SUCCEDDFULL_NOTIFICATION = "The product was successfully saved.";
    public static final String TITLE = "title";
    public static final String ORDER_URL = "/dashboard/#/pages/orders/";
    public static final String CANCEL_REASON = "because";
    public static final String ORDER_SUCCESSFULL_UPDATE = "Order status was successfully updated.";
    public static final String CONTENT_TYPE= "application/json";
    public static final String CHANGE_DROPOFF_LOCATION = "/delivery/mgmt/v2/orders/";
    public static final String DROPOFF_URL_LAST = "/dropoff/?createOutOfMaxArea=false";
    public static final String FIND_PRODUCT = "/catalog/search/v4/products/";
    public static final String CURRENCY_ENDPOINT ="/?currency=SAR";
    public static final String AUTHORIZATION = "Authorization";
    public static final String ACCEPT_LANG ="Accept-Language";
    public static final String EN = "en";





}
